package ownjava;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class RandomHelper {

    public static String getRandomSequence() {
        Random random = new Random();
        ArrayList<Integer> values = new ArrayList<Integer>();
        while (values.size() < 9) {
            int rnd = random.nextInt(1000);
            if (!values.contains(rnd)) {
                values.add(rnd);
            }
        }
        Integer[] sorted = new Integer[9];
        values.toArray(sorted);
        Arrays.sort(sorted);

        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < 9; i++) {
            sb.append(values.indexOf(sorted[i]));
        }
        return sb.toString();
    }
}
