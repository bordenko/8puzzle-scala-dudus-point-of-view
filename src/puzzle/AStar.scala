package puzzle

object AStar {

  def searchTree(start: State): Option[List[State]] = {
    var openSet = Set(Node(start))
    while (!openSet.isEmpty) {
      val current = openSet.minBy(_.getFCost)
      if (current.state.isGoal) {
        return Some(current.path.map(_.state))
      }
      openSet -= current
      current.state.neighbours.foreach((x: State) => openSet += Node(x, Some(current)))
    }
    return None
  }

  def searchGraph(start: State): Option[List[State]] = {
    var fringe = Set(Node(start))
    var allSet = Set(start)
    var loops = 0;
    while (!fringe.isEmpty && loops < 100000) {
      val current = fringe.minBy(_.getFCost)
      if (current.state.isGoal) {
        return Some(current.path.map(_.state))
      }
      fringe -= current
      current.state.neighbours.foreach {x =>
        if (!allSet.contains(x)) {
          fringe += Node(x, current); allSet += x
        }
      }
      loops += 1
    }
    return None
  }

}
