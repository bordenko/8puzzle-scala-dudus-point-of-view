package puzzle

object Node {
  def apply(state: State, parent: Node): Node = new Node(state, Some(parent))
  def apply(state: State): Node = new Node(state, parent = None)
}

case class Node(state: State, parent: Option[Node]) {
  val cost: Int = parent.map(_.cost + 1).getOrElse(0)
  val getFCost: Int = cost + state.manhattanDistance

  /**
   * @return the path from the root to this node
   */
  def path: List[Node] = {
    def collect(ns: List[Node], no: Option[Node]): List[Node] = no match {
      case None => ns
      case Some(node) => collect(node :: ns, node.parent)
    }
    collect(List.empty, Some(this))
  }
}
