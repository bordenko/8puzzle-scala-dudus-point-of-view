package puzzle

import ownjava.RandomHelper

object State {
  def apply(digits: String) = new State(digits.map(x => Integer.parseInt(x toString)).toList)
  def randomState: State = apply(RandomHelper.getRandomSequence)
}

/**
 * Created by adam on 1/27/14.
 */
case class State(digits: List[Int]) {

  val cols: Int = Math.sqrt(digits.size).toInt
  val rows: Int = cols

  val manhattanDistance: Int = {
    def distance(v: Int, i: Int): Int = {
      val r = Math.abs(i/rows - v/rows); val c = Math.abs(i%cols - v%cols); r + c
    }
    var index = 0; var sum = 0
    digits.foreach {x => sum += distance(x, index); index += 1}
    sum
  }
  
  val isGoal: Boolean = manhattanDistance == 0

  def neighbours: List[State] = {
    val iZero = digits.indexOf(0)
    val posZero = (iZero / rows, iZero % cols)
    val neighbourPositions = List(posZero.up, posZero.down, posZero.left, posZero.right).filter(p=>p.onBoard(rows, cols))
    val neighbourIndexes = neighbourPositions.map(p=>p._1 * rows + p._2)
    neighbourIndexes.map(i=>State(digits.swap(iZero, i)))
  }

  override def toString: String = "State(" + digits.mkString(",") + ")"

  def print: String = {
    val rows = digits.grouped(cols).map(_.mkString(",")).mkString("\n")
    val separatorLine = (1 to cols).map(i=>"-").mkString("-")
    rows + "\n" + separatorLine
  }

}
