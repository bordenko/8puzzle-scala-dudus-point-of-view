package object puzzle {

  /**
   * Extends List functions
   * @param l list object
   * @tparam T type of elements
   */
  implicit class ListCompanionOps[T](l: List[T]) {
    /**
     * Create a new list by swapping element at i1 over element at i2 and vice versa
     * Example: List(1,2,3,4,5,6,7).swap(2,5) == List(1,2,6,4,5,3,7)
     * @param i1 index of element to swap over element at i2
     * @param i2 index of element to swap over element at i1
     * @return Copy of l but i1 swapped over i2
     */
    def swap(i1: Int, i2: Int): List[T] =
      (for (i <- 0 to (l.size -1)) yield if (i == i1) l(i2) else if (i == i2) l(i1) else l(i)).toList
  }

  /**
   * Tun a pair of integers into grid position
   * @param pos
   */
  implicit class GridPosCompanionOps(pos: (Int, Int)) {
    /**
     * @return the position above
     */
    def up = (pos._1 - 1, pos._2)

    /**
     * @return the position below
     */
    def down = (pos._1 + 1, pos._2)

    /**
     * @return the position on the left
     */
    def left = (pos._1, pos._2 - 1)

    /**
     * @return the position on the right
     */
    def right = (pos._1, pos._2 + 1)

    /**
     * Check if this position is on the given board (upper-left is (0,0))
     * @param rows number of rows of the board
     * @param cols number of columns of the board
     * @return true if this position is on the given board, false otherwise
     */
    def onBoard(rows: Int, cols: Int) = pos._1 >= 0 && pos._1 < rows && pos._2 >= 0 && pos._2 < cols
  }

  /**
   * Print the resolution path of the puzzle
   * @param sl
   */
  def printPath(sl: List[State]) {
    println("Steps: " + (sl.size - 1))
    sl.foreach(x => println(x.print))
  }

}

